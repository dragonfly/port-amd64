/*
 * Default target format spec for the assembler
 *
 * $DragonFly: src/gnu/usr.bin/binutils217/as/amd64-dragonfly/targ-env.h,v 1.1 2007/04/13 12:24:32 corecode Exp $
 */

#define ELF_TARGET_FORMAT	"elf64-amd64-dragonfly"
#define LOCAL_LABELS_DOLLAR 1
#define LOCAL_LABELS_FB 1

#include "obj-format.h"

