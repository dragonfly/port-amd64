.\" Copyright (c) 2001-2003, Intel Corporation
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions are met:
.\"
.\" 1. Redistributions of source code must retain the above copyright notice,
.\"    this list of conditions and the following disclaimer.
.\"
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\"
.\" 3. Neither the name of the Intel Corporation nor the names of its
.\"    contributors may be used to endorse or promote products derived from
.\"    this software without specific prior written permission.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
.\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
.\" LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
.\" CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
.\" SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
.\" INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
.\" CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
.\" ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
.\" POSSIBILITY OF SUCH DAMAGE.
.\"
.\" * Other names and brands may be claimed as the property of others.
.\"
.\" $FreeBSD: src/share/man/man4/em.4,v 1.2.2.6 2003/02/01 01:21:31 pdeuskar Exp $
.\" $DragonFly: src/share/man/man4/em.4,v 1.9 2007/03/08 11:20:56 swildner Exp $
.\"
.Dd March 4, 2007
.Dt EM 4
.Os
.Sh NAME
.Nm em
.Nd "Intel(R) PRO/1000 gigabit Ethernet driver"
.Sh SYNOPSIS
.Cd "device em"
.Sh DESCRIPTION
The
.Nm
driver provides support for PCI gigabit Ethernet adapters based on
the Intel 82540, 82541, 82542, 82543, 82544, 82546, and 82547 Ethernet
controller chips.
The driver does not support Transmit/Receive checksum offload and
Jumbo Frames on adapters based on the Intel 82542 and 82573 LAN controllers.
For a list of supported adapters, see the
.Pa README
included with the driver.
.Pp
For questions related to hardware requirements,
refer to the documentation supplied with your Intel PRO/1000 adapter.
All hardware requirements listed apply to use with
.Dx .
.Pp
Support for Jumbo Frames is provided via the interface MTU setting.
Selecting an MTU larger than 1500 bytes with the
.Xr ifconfig 8
utility configures the adapter to receive and transmit Jumbo Frames.
The maximum MTU setting for Jumbo Frames is 16110.
This value coincides with the maximum Jumbo Frames size of 16128.
Some Intel gigabit adapters that support Jumbo Frames have a frame size
limit of 9238 bytes, with a corresponding MTU size limit of 9216 bytes.
The adapters with this limitation are based on the Intel(R)
82571EB, 82572EI, 82573L, 82563EB and 82564EI LAN controllers.
.Pp
The following Intel gigabit LAN devices do not support Jumbo Frames:
.Pp
Intel(R) PRO/1000 PM Network Connection
.Pp
Adapters based on the Intel(R) 82542 and 82573 LAN controllers.
.Pp
This driver version supports VLANs.
For information on enabling VLANs,
see the
.Pa README .
The
.Nm
driver supports the following media types:
.Bl -tag -width ".Cm 10baseT/UTP"
.It Cm autoselect
Enables auto-negotiation for speed and duplex.
.It Cm 10baseT/UTP
Sets 10Mbps operation.
Use the
.Cm mediaopt
option to select
.Cm full-duplex
mode.
.It Cm 100baseTX
Sets 100Mbps operation.
Use the
.Cm mediaopt
option to select
.Cm full-duplex
mode.
.It Cm 1000baseSX
Sets 1000Mbps operation.
Only
.Cm full-duplex
mode is supported at this speed.
.It Cm 1000baseT
Sets 1000Mbps operation.
Only
.Cm full-duplex
mode is supported at this speed.
.El
.Pp
The
.Nm
driver supports the following media options:
.Bl -tag -width ".Cm full-duplex"
.It Cm full-duplex
Forces full-duplex operation
.It Cm half-duplex
Forces half-duplex operation.
.El
.Pp
Only use
.Cm mediaopt
to set the driver to
.Cm full-duplex .
If
.Cm mediaopt
is not specified, the driver defaults to
.Cm half-duplex .
.Pp
For more information on configuring this device, see
.Xr ifconfig 8 .
.Sh TUNABLES
.Bl -tag -width ".Va hw.em.int_throttle_ceil"
.It Va hw.em.int_throttle_ceil
Hardware interrupt throttling rate.
The default value is 10000Hz.
.It Va hw.em.rxd
Number of receive descriptors allocated by the driver.
The default value is 256.
The 82542 and 82543-based adapters can handle up to 256 descriptors,
while others can have up to 4096.
.It Va hw.em.txd
Number of transmit descriptors allocated by the driver.
The default value is 256.
The 82542 and 82543-based adapters can handle up to 256 descriptors,
while others can have up to 4096.
.It Va hw.em.rx_int_delay
This value delays the generation of receive interrupts in units of
1.024 microseconds.
The default value is 0, since adapters may hang with this feature
being enabled.
.It Va hw.em.rx_abs_int_delay
If
.Va hw.em.rx_int_delay
is non-zero, this tunable limits the maximum delay in which a receive
interrupt is generated.
.It Va hw.em.tx_int_delay
This value delays the generation of transmit interrupts in units of
1.024 microseconds.
The default value is 64.
.It Va hw.em.tx_abs_int_delay
If
.Va hw.em.tx_int_delay
is non-zero, this tunable limits the maximum delay in which a transmit
interrupt is generated.
.El
.Sh DIAGNOSTICS
.Bl -diag
.It "em%d: Unable to allocate bus resource: memory"
A fatal initialization error has occurred.
.It "em%d: Unable to allocate bus resource: interrupt"
A fatal initialization error has occurred.
.It "em%d: watchdog timeout -- resetting"
The device has stopped responding to the network, or there is a problem with
the network connection (cable).
.El
.Sh SUPPORT
For additional information regarding building and installation,
see the
.Pa README
included with the driver.
For general information and support,
go to the Intel support website at:
.Pa http://support.intel.com .
.Pp
If an issue is identified with the released source code on the supported kernel
with a supported adapter, email the specific information related to the
issue to
.Aq freebsdnic@mailbox.intel.com .
.Sh SEE ALSO
.Xr arp 4 ,
.Xr gx 4 ,
.Xr ifmedia 4 ,
.Xr netintro 4 ,
.Xr ng_ether 4 ,
.Xr polling 4 ,
.Xr vlan 4 ,
.Xr ifconfig 8
.Sh HISTORY
The
.Nm
device driver first appeared in
.Fx 4.4 .
.Sh AUTHORS
The
.Nm
driver was written by
.An Intel Corporation Aq freebsdnic@mailbox.intel.com .
