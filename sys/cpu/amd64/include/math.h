/* 
 * $NetBSD: math.h,v 1.2 2003/10/28 00:55:28 kleink Exp $
 * $DragonFly: src/sys/cpu/amd64/include/math.h,v 1.1 2007/08/21 19:40:24 corecode Exp $
 */

#ifndef _CPU_MATH_H_
#define _CPU_MATH_H_

#define	__HAVE_LONG_DOUBLE
#define	__HAVE_NANF

#endif
